package com.jjimenez.producer;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

public class StudentsProducer {
    public static void main(String[] args) {

        Producer <String, String> producer = new KafkaProducer<String, String> (generate_Config());

        int i = 0;
        while (true) {
            System.out.println("Producing message: " +i);

            try {
                producer.send(new_Record("jesus","male",33));
                Thread.sleep(100);
                producer.send(new_Record("olga","female",29));
                Thread.sleep(100);
                producer.send(new_Record("javier","male",34));
                Thread.sleep(100);
                producer.send(new_Record("cristina","female",32));
                Thread.sleep(100);
                producer.send(new_Record("david","male",18));
                Thread.sleep(100);
                producer.send(new_Record("irene","female",7));
                Thread.sleep(100);
                i+=1;

                if (i == 10)
                    break;

            } catch (InterruptedException e) {
                break;
            }
        }


        producer.close();
    }

    private static Properties generate_Config () {
        String server = "127.0.0.1:9092";
        String ack_config = "all";
        String retries = "3";
        String linger_ms_config = "1";
        String idempotence_config = "true";

        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.ACKS_CONFIG, ack_config);
        props.setProperty(ProducerConfig.RETRIES_CONFIG, retries);
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, linger_ms_config);
        props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, idempotence_config);

        return props;
    }

    private static ProducerRecord <String,String> new_Record (String name, String gender, int age) {
        ObjectNode json_data = JsonNodeFactory.instance.objectNode();

        json_data.put("name",name);
        json_data.put("gender",gender);
        json_data.put("age", age);

        return new ProducerRecord <> ("students_in",name,json_data.toString());

    }
}
