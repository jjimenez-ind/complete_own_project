package com.jjimenez.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class StudentsConsumer {
    public static void main(String[] args) {

        StreamsBuilder builder = new StreamsBuilder();
        KStream <String, String> text_lines = builder.stream("students_in");

        System.out.println("init");
        KStream <String, String> filtered_datas = text_lines
                .filter((key,value) -> {
                    System.out.println(value);
                    return false;
                });
        System.out.println("end");
        filtered_datas.to("students_out");

        KafkaStreams streams = new KafkaStreams(builder.build(), generate_Properties());

        streams.cleanUp();
        streams.start();
        streams.localThreadsMetadata().forEach(data -> System.out.println(data));
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private static Properties generate_Properties () {
        String id = "seville_classroom";
        String server = "127.0.0.1:9092";
        String offset = "earliest";

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, id);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offset);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        return props;
    }
}
